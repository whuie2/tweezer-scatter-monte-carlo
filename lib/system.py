"""
All quantities in MKS.
"""

from __future__ import annotations
from math import pi
from enum import IntEnum
from collections import defaultdict
import numpy as np
from .phys import c, hbar
from .scatter import scattering_rate, population
from .probability import sample_lifetime

TWEEZER_WAIST = 600e-9
TWEEZER_WAVELENGTH_REPUMP = 772e-9
TWEEZER_WAVELENGTH_NO_REPUMP = 760e-9

BACKGROUND_MEAN = 16.0 # in 100 ms
BACKGROUND_STD = 11.5 # in 100 ms

COLLECTION_EFFICIENCY = 0.015

class State(IntEnum):
    s_1S0 = 0
    s_1P1 = 1
    s_3P0 = 2
    s_3P1 = 3
    s_3P2 = 4
    s_3S1 = 5
    s_3D1 = 6
    s_3D2 = 7

# transition frequencies in Hz
_frequencies = {
    State.s_1S0: {
        State.s_1P1: c / 399e-9,
        State.s_3P1: c / 556e-9,
    },
    State.s_3P1: {
        State.s_3S1: c / 680.1e-9,
        State.s_3D1: c / 1538.9e-9,
        State.s_3D2: c / 1479.3e-9,
    },
}
# symmeterize
frequencies = _frequencies.copy()
for s1, s2f in _frequencies.items():
    for s2, f in s2f.items():
        if s2 not in _frequencies.keys():
            frequencies[s2] = dict()
        frequencies[s2][s1] = f

# transition linewidths in Hz
_linewidths = {
    State.s_1S0: {
        State.s_1P1: 30e6,
        State.s_3P1: 182e3,
    },
    State.s_3P1: {
        State.s_3S1: 27e6 / (2.0 * pi),
        State.s_3D1: 1e6 / (2.0 * pi),
        State.s_3D2: 2e6 / (2.0 * pi),
    },
}
# symmeterize
linewidths = _linewidths.copy()
for s1, s2y in _linewidths.items():
    for s2, y in s2y.items():
        if s2 not in _linewidths.keys():
            linewidths[s2] = dict()
        linewidths[s2][s1] = y

# branching ratios (dimensionless)
branch_ratios = {
    State.s_3P1: {
        State.s_3S1: (9.6e6 + 37e6) / (27e6 + 37e6 + 9.6e6)
    },
    State.s_3S1: {
        State.s_3P0: (9.6e6) / (27e6 + 37e6 + 9.6e6)
    },
}

populations = {
    s1: {
        s2: lambda s, detuning: population(s, detuning, l)
        for s2, l in s2l.items()
    }
    for s1, s2l in linewidths.items()
}

scattering_rates = {
    s1: {
        s2: lambda s, detuning: scattering_rate(s, detuning, l)
        for s2, l in s2l.items()
    }
    for s1, s2l in linewidths.items()
}

def tweezer_scattering_rate_3P1(
    power: float,
    wavelength: float,
    waist: float
) -> float:
    intensity = 2.0 * power / (pi * waist**2)
    w_tweezer = (2.0 * pi) * c / wavelength
    ww = [
        frequencies[State.s_3P1][s] * (2.0 * pi)
        for s in [State.s_3S1, State.s_3D1, State.s_3D2]
    ]
    yy = [
        linewidths[State.s_3P1][s] * (2.0 * pi)
        for s in [State.s_3S1, State.s_3D1, State.s_3D2]
    ]
    return (
        (3.0 * pi * c**2 / (2.0 * hbar))
        * sum(
            (y**2 / w**3)
            * (w_tweezer / w)**3
            * (1.0 / (w - w_tweezer) + 1.0 / (w + w_tweezer))**2
            for w, y in zip(ww, yy)
        )
        * intensity
    )

def lifetime_3P1_with_repump(
    s_image: float,
    det_image: float,
    p_tweezer: float
) -> float:
    return 1.0 / (
        populations[State.s_1S0][State.s_3P1](s_image, det_image)
        * tweezer_scattering_rate_3P1(
            p_tweezer,
            TWEEZER_WAVELENGTH_REPUMP,
            TWEEZER_WAIST
        )
        * branch_ratios[State.s_3S1][State.s_3P0]
    )

def lifetime_3P1_without_repump(
    s_image: float,
    det_image: float,
    p_tweezer: float
) -> float:
    return 1.0 / (
        populations[State.s_1S0][State.s_3P1](s_image, det_image)
        * tweezer_scattering_rate_3P1(
            p_tweezer,
            TWEEZER_WAVELENGTH_NO_REPUMP,
            TWEEZER_WAIST
        )
        * branch_ratios[State.s_3P1][State.s_3S1]
    )

def sample_background(
    imaging_time: float,
    shape: (int, ...)=None
) -> float | np.ndarray:
    return np.random.normal(
        loc=BACKGROUND_MEAN * imaging_time / 100e-3,
        scale=BACKGROUND_STD * np.sqrt(imaging_time / 100e-3),
        size=shape
    )

def sample_atoms(
    nbar: float,
    parity_project: bool=True,
    shape: (int, ...)=None
) -> float | np.ndarray:
    n = np.random.poisson(nbar, size=shape)
    if parity_project:
        return n % 2
    else:
        return n

def sample_scatter_3P1(
    s_image: float,
    det_image: float,
    lifetime: float,
    imaging_time: float,
    shape: (int, ...)=None
) -> float | np.ndarray:
    t = np.array([
        min(_t, imaging_time)
        for _t in sample_lifetime(lifetime, shape)
    ])
    R = scattering_rates[State.s_1S0][State.s_3P1](s_image, det_image)
    # print(R)
    p = R * t * COLLECTION_EFFICIENCY
    dp = np.random.normal(loc=0.0, scale=np.sqrt(p), size=shape) # shot noise
    return p + dp

def sample_photons_3P1(
    nbar: float,
    s_image: float,
    det_image: float,
    lifetime: float,
    imaging_time: float,
    parity_project: bool=True,
    shape: (int, ...)=None
) -> float | np.ndarray:
    p = (
        sample_atoms(nbar, parity_project, shape)
        * sample_scatter_3P1(s_image, det_image, lifetime, imaging_time, shape)
    )
    return p
