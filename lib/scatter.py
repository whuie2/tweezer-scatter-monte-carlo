"""
All quantities in MKS.
"""

from __future__ import annotations
from math import pi

def population(s: float, detuning: float, linewidth: float) -> float:
    return 0.5 * s / (1.0 + s + (detuning / (linewidth / 2.0))**2)

def scattering_rate(s: float, detuning: float, linewidth: float) -> float:
    return linewidth * population(s, detuning, linewidth)

