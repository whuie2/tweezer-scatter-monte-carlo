from __future__ import annotations
import numpy as np

def lifetime_pdf(tau: float, t: float | np.ndarray) -> float | np.ndarray:
    return np.exp(-t / tau) / tau

def lifetime_cdf(tau: float, t: float | np.ndarray) -> float | np.ndarray:
    return 1.0 - np.exp(-t / tau)

def sample_lifetime(tau: float, shape: (int, ...)=None) -> float | np.ndarray:
    return -tau * np.log(1.0 - np.random.random(size=shape))

