import numpy as np
from lib.system import *
import lib.pyplotdefs as pd
pd.pp.rcParams["font.size"] = 8.0

N = 1000000
nbar = 100.0
pp = True
t_image = 100e-3
s_image = 2.0
det_image = 100e3
p_tweezer = 25e-3 * 0.4
background_mod = 1.0
lifetime_mod = 1.0

data = (
    sample_photons_3P1(
        nbar,
        s_image,
        det_image,
        lifetime_3P1_without_repump(
            s_image,
            det_image,
            p_tweezer,
        ) * lifetime_mod,
        t_image,
        pp,
        shape=N,
    )
    + sample_background(
        t_image * background_mod,
        shape=N
    )
)

# print(
#     lifetime_3P1_without_repump(
#         s_image,
#         det_image,
#         p_tweezer,
#     )
# )

# background = sample_background(
#     t_image * background_mod,
#     shape=int(N * (1.0 - fill_fraction))
# )
# photons = (
#     sample_background(
#         t_image * background_mod,
#         shape=int(N * fill_fraction)
#     )
#     + sample_scatter_3P1(
#         s_image,
#         det_image,
#         lifetime_3P1_without_repump(
#             s_image,
#             det_image,
#             p_tweezer,
#         ) * lifetime_mod,
#         t_image,
#         shape=int(N * fill_fraction)
#     )
# )

# data = np.append(background, photons)
np.save("roi_totals_mc.npy", data)

bin_size = 3

(pd.Plotter()
    .hist(
        data,
        bins=np.arange(-3 * bin_size, data.max() + 2 * bin_size, bin_size),
        density=True,
        edgecolor="k",
        linewidth=0.5,
    )
    .ggrid().grid(False, which="both")
    .set_xlabel("Photons")
    .set_ylabel("Prob. density")
    .savefig("hist_mc.png")
)

